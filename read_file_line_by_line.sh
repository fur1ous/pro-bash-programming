#!/bin/bash
#: Title	: read_file_line_by_line.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Read file line by line.
#: Options	: None

while IFS= read -r line
do
	echo $line
done < $1
