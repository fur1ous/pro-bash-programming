#!/bin/bash
#: Title	: dname.sh
#: Date		: 2018-08-22
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Print the directory portion of a file path.
#: Options	: None

case $1 in
	*/*) printf "%s\n" "${1%/*}" ;;
	*) [ -e "$1" ] && printf "%s\n" "$PWD" || echo '.' ;;
esac
