#!/bin/bash
#: Title	: check_range.sh
#: Date		: 2018-08-23
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Check Whether an Integer Is Within a Specified Range.
#: Options	: None

rangecheck() #@ USAGE: rangecheck int [low [high]]
	if [ "$1" -lt ${2:-10} ]
	then
		return 1
	elif [ "$1" -gt ${3:-20} ]
	then
		return 2
	else
		return 0
	fi
