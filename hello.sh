#!/bin/bash
#: Title	: hello.sh
#: Date		: 2018-08-18
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: print Hello and the first command-line argument
#: Options	: None

printf "Hello, %s!\n" "$1"
