#!/bin/bash
#: Title	: infinite_loop.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Run some commands indefinite number of times.
#: Options	: None

while true ## ':' can be used instead of true
do
	read x
done
