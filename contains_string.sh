#!/bin/bash
#: Title	: contains_string.sh
#: Date		: 2018-08-20
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Does one string contain another?
#: Options	: None

case $1 in
	*"$2"*) echo true ;;
	*) echo false ;;
esac
