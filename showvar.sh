#!/bin/bash
#: Title	: var_in_environment.sh
#: Date		: 2018-08-22
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Tell whether the variable is in the environment and what it
#:+		: contains.
#: Options	: None

if [[ ${x+X} = X ]] ## If $x is set
then
	if [[ -n $x ]] ## if $x is not empty
	then
		printf " \$x = %s\n" "$x"
	else
		printf " \$x is set but empty\n"
	fi
else
	printf " %s is not set\n" "\$x"
fi
