#!/bin/bash
#: Title	: read_check_input.sh
#: Date		: 2018-08-20
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Check weather the name was entered during input.
#: Options	: None

read -p "Enter name? " name
if [ -z "$name" ]
then
	echo "No name entered" >&2
	exit 1 ## Set a failed return code
fi
