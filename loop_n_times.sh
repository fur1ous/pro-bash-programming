#!/bin/bash
#: Title	: loop_n_times.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Run command specific number of times, defined by variable n.
#: Options	: None

n=1
while [ $n -le 10 ]
do
	echo $n
	n=$(( $n + 1 ))
done
