#!/bin/bash
#: Title	: loop_through_countries.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Loop through country names using FOR loop.
#: Options	: None

for var in Canada USA Mexico
do
	printf "%s\n" "$var"
done
