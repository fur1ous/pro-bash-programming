#!/bin/bash
#: Title	: extract_second_octet.sh
#: Date		: 2018-08-23
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Given var=192.168.0.123, write a script that uses parameter
#:+		: expansion to extract the second number, 168.
#: Options	: None

var=192.168.0.123
printf "%s\n" "${var:4:3}"
