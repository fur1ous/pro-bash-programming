# Read and check input
read name
if [[ -z $name ]]
then
	echo "No name entered" >&2
	exit 1 ## Set a failed return code
fi


# Prompt for a number and check that it is not a greater than ten
printf "Enter a number not greater than 10: "
read number
if (( number > 10 ))
then
	printf "%d is too big\n" "$number" >&2
	exit 1
else
	printf "You entered %d\n" "$number"
fi


# Prompt for a number and check that it is within a given range
printf "Enter a number between 10 and 20 inclusive: "
read number
if (( number < 10 ))
then
	printf "%d is too low\n" "$number" >&2
	exit 1
elif (( number > 20 ))
then
	printf "%d is too high\n" "$number" >&2
	exit 1
else
	printf "You entered %d\n" "$number"
fi
