#!/bin/bash
#: Title	: loop_unitl.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Execute the commands as long as the condition fails.
#: Options	: None

n=1
until [ $n -gt 10 ]
do
	echo $n
	n=$(( $n + 1 ))
done

