#!/bin/bash
#: Title	: sc.sh
#: Date		: 2018-08-19
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 2018-08-19
#: Description	: Write a script, using $RANDOM, to write the following output
#:+		: both to a file and to a variable. The following numbers are
#:+		: only to show the format; your script should produce different
#:+		: numbers:
#:+		:	1988.2365
#:+		:	13798.14178
#:+		:	10081.134
#:+		:	3816.15098
#: Options	: None

for i in {0..7}
do
	r+=${RANDOM:+ $RANDOM}
done
printf -v var "%5.5s.%4.4s\n" $r
