#!/bin/bash
#: Title	: check_number_greater.sh
#: Date		: 2018-08-20
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Prompt for a number and check that it is not greater than 10.
#: Options	: None

read -p "Enter some integer number not greater than 10? " number
if (( number > 10 ))
then
	printf "%d is too big\n" "$number" >&2
	exit 1
else
	printf "You entered %d\n" "$number"
fi
