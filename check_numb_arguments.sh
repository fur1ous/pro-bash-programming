#!/bin/bash
#: Title	: check_num_arguments.sh
#: Date		: 2018-08-20
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Check whether there is correct number of arguments.
#: Options	: None

case $# in
	3) ;; ## we need 3 args, so do nothing
	*) printf "%s\n" "Please provide three names" >&2
	   exit 1
	   ;;
esac
