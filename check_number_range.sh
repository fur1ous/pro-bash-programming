#!/bin/bash
#: Title	: check_number_range.sh
#: Date		: 2018-08-20
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Prompt for a number between 10 and 20 inclusive.
#: Options	: None

read -p "Enter a number between 10 and 20 inclusive? " number
if (( number < 10 ))
then
	printf "%d is too low\n" "$number" >&2
	exit 1
elif (( number > 20 ))
then
	printf "%d is too high\n" "$number" >&2
	exit 1
else
	printf "You entered %d\n" "$number"
fi
